# Vue 3 + TypeScript + Vite + Three + antv G2 实战智慧城市项目

开发框架版本
1."vue": "^3.2.47"
2."@antv/g2plot": "^2.4.29",
3."typescript": "^5.0.2",
4."vite": "^4.3.0",
5."@types/three": "^0.150.2",

开发工具
vscode

开发环境
node 16.0.0
npm 7.10.0

运行效果
![输入图片说明](public/img/%E9%A3%9E%E4%B9%A620230424-155909.jpg)


https://mp.weixin.qq.com/s/w1TI4syhvtKWoA928kpO1w
